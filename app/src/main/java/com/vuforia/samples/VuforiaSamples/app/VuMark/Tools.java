package com.vuforia.samples.VuforiaSamples.app.VuMark;

/**
 * Created by NS on 04.01.2017.
 */

import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Class with tools methods
 * Created by NS on 04.01.2017.
 */
public class Tools {
    private static final String TAG = "=====";

    /**
     * Save file to disk
     * @param bytes
     * @param folder
     * @param fileName
     * @return
     */
    public static boolean saveFile(byte[] bytes, String folder, String fileName, boolean append) {
        String path = Environment.getExternalStorageDirectory()+"/"+ folder;
        String fullPath = path + "/" + fileName;

        try {
            File dir = new File(path);
            dir.mkdirs();
            myLog("Create dir: " + path);
        } catch (Exception e) {
            myLog("*** Error creating dir: " + path);
            return false;
        }

        File file = new File(fullPath);
        myLog("file to write=" + file.toString());

        if (!append) {
            if (file.exists()) {
                file.delete();
            }
        }

        try {
            FileOutputStream fileOuputStream =  new FileOutputStream(fullPath, append);
            if (bytes != null) {
                fileOuputStream.write(bytes);
            }
            fileOuputStream.close();
        }
        catch (Exception e) {
            myLog("*** Error writing: " + fullPath);
            return false;
        }
        return true;
    }


    /**
     * Load file from disk
     * @param folder
     * @param name
     * @return
     */
    public static byte[] loadFile(String folder, String name) {
        String path = Environment.getExternalStorageDirectory()+"/"+ folder + "/" + name;
        File file = new File(path);
        myLog("file=" + file.toString());

        int size = (int) file.length();
        if (size == 0) {
            return null;
        }
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (IOException e) {
            return null;
        }
        return bytes;
    }

    public static void myLog(String str) {
        Log.d(TAG, str);
    }
}
